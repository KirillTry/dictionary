dict = {
    "Яблоко" : "Apple",
    "Люди" : "People",
    "Имя" : "Name",
    "Друзья" : "Friend",
    "Кошка" : "Cat",
    "Ананас" : "Pineapple",
    "Жизнь" : "Life",
    "Мир" : "World",
    "Цена" : "Price",
    "Ответ" : "Answer",
    "Кокос" : "Coconut",
    "Шоколад" : "Chocolate",
    "Луна" : "Moon",
    "Солнце" : "Sun",
    "Мысль" : "Thought",
    "Город" : "City",
    "Дерево" : "Tree",
    "История" : "Story",
    "Море" : "Sea",
    "Лицо" : "Face",
    "Книга" : "Book",
    "Наука" : "Science",
    "Вместе" : "Together",
    "Музыка" : "Music",
    "Просто" : "Plain",
    "Птица" : "Bird",
    "Собака" : "Dog",
    "Вопрос" : "Question",
    "Говорил" : "Told",
    "Остров" : "Island",
    "Система" : "System",
    "Компьютер" : "Computer",
    "Золото" : "Gold",
    "Серебро" : "Silver",
    "Бронза" : "Bronze",
    "Белый" : "White",
    "Черный" : "Black",
    "Красный" : "Red",
    "Желтый" : "Yellow",
    "Оранжевый" : "Orandge",
    "Зеленый" : "Green",
    "Синий" : "Blue",
    "Фиолетовый" : "Purple",
    "Розовый" : "Pink",
    "Коричневый" : "Brown",
    "Серый" : "Grey",
    "Кровать" : "Bed",
    "Брат" : "Brother",
    "Верить" : "Believe",
    "Банан" : "Banana",
    "Речь" : "Speech",
    "Сахар" : "Sugar",
    "Соль" : "Salt",
    "Зубы" : "Teeth",
    "Кислород" : "Oxygen",
    "Рынок" : "Market",
    "Правильный" : "Proper",
    "Аккорд" : "Chord",
    "Население" : "Population",
    "Враг" : "Enemy",
    "Место" : "Spot",
    "Мастер" : "Master",
    "Часы" : "Clock",
    "Планета" : "Planet",
    "Квадрат" : "Square",
    "Крест" : "Cross",
    "Треугольник" : "Triangle",
    "Создать" : "Create",
    "Кукуруза" : "Corn",
    "Арбуз" : "Watermelon",
    "Кролик" : "Rabbit",
    "Белка" : "Squirlle",
    "Страус" :"Ostrich",
    "Стих" : "Poem",
    "Капитан" : "Captain",
    "Насекомое" : "Insect",
    "Животное" : "Animal",
    "Рыба" : "Fish",
    "Кольцо" : "Ring",
    "Доктор" : "Doctor",
    "Богатый" : "Rich",
    "Бедный" : "Poor",
    "Сложный" : "Difficult",
    "Легкий" : "Light",
    "Средний" : "Average",
    "Класс" : "Class",
    "Ручка" : "Pen",
    "Карандаш" : "Pencil",
    "Слон" : "Elephant",
    "Лев" : "Lion",
    "Тигр" : "Tiger",
    "Пантера" : "Panther",
    "Пума" : "Puma",
    "Змея" : "Snake",
    "Леопард" : "Leopard",
    "Гепард" : "Cheetah",
    "Паук" : "Spider",
    "Попугай" : "Parrot",
    "Орел" : "Eagle",
    "Медведь" : "Bear",
    "Горох" : "Peas",
    "Рис" : "Rice",
    "Помидор" : "Tomato",
    "Лук" : "Onion",
    "Гречка" : "Buckwheat",
    "Картофель" : "Potato",
    "Овсянка" : "Oatmeal",
    "Салат" : "Salad",
    "Капуста" : "Cabbage",
    "Морковь" : "Carrot",
    "Страна" : "Country",
    "Текст" : "Text",
    "Футбол" : "Football",
    "Баскетбол" : "Basketball",
    "Киберспорт" : "Esport",
    "Шахматы" : "Chess",
    "Бокс" : "Boxing",
    "Борьба" : "Struggle",
    "Торт" : "Cake",
    "Печенье" : "Cookie",
    "Леденец" : "Lollipop",
    "Мороженое" : "Ice cream",
    "Конфета" : "Candy",
    "Сладкий" : "Sweet",
    "Кислый" : "Sour",
    "Горький" : "Bitter",
    "Острый" : "Spicy",
    "Соленый" : "Salty",
    "Клубника" : "Strawberry",
    "Виноград" : "Grape",
    "Вишня" : "Cherry",
    "Брусника" : "Cowberry",
    "Дыня" : "Melon",
    "Лимон" : "Lemon",
    "Малина" : "Raspberry",
    "Стул" : "Chair",
    "Стол" : "Table",
    "Кактус" : "Cactus"
}
print("=" * 10, "Словарь v-1.0", "=" * 10)
help = """
s - Поиск слова по словарю
a - Добавить новое слово
r - Удалить слово
k - Вывод всех ключей в словаре
d - Вывод всего словаря
h - Вывод меню
q - Выход
"""

choice = ""
while choice != "q":
    choice = input("(h - Справка >>)")
    if choice == "s":
        world = input("Введите слово: ")
        res = dict.get(world, "Нет такого слова!")
        print(res)
    elif choice == "a":
        word = input("Введите слово: ")
        value = input("Введите перевод: ")
        dict[word] = value
        print("Слово добавлено!")
    elif choice == "r":
        word = input("Введите слово: ")
        del dict[word]
        print("Слово ", word, " удалено")
    elif choice == "k":
        print(dict.keys())
    elif choice == "d":
        for i in dict:
            print(i, ": ", dict[i])
    elif choice == "h":
        print(help)
    elif choice == "q":
        continue
    else:
        print("!Нераспознанная команда! Введите h для открытия меню!")